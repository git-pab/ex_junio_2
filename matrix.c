#include <stdio.h>
#include "matrix.h"
#include "vector.h"

void print_matrix (int** m, int num_rows, int num_cols) {
  printf("[");
  print_vector(m[0], num_cols);
  for (int i = 1; i < num_rows; i++) {
    printf(", ");
    print_vector(m[i], num_cols);
  }
  printf("]");
}

void print_matrix_alt (int* m, int num_rows, int num_cols) {
  printf("[");
  print_vector(m, num_cols);
  for (int i = 1; i < num_rows; i++) {
    printf(", ");
    print_vector(m+i*num_cols, num_cols);
  }
  printf("]");
}
