#ifndef SVECTOR_H
#define SVECTOR_H

typedef struct {
  int* data;
  int size;
} svector_t;

void print_svector (svector_t* v);

#endif //SVECTOR_H
