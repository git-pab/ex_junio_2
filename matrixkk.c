#include <stdlib.h>
#include <stdio.h>
#include "matrixkk.h"
#include "tripla_aij.h"

matrixkk_t* read_matrixkk (const char*  file_name) {
  //allocate matrixkk on the heap in order to return it
  matrixkk_t* m = (matrixkk_t*) malloc (sizeof(matrixkk_t));
  //declare a file pointer and open file stream
  FILE *fp;
  fp = fopen(file_name, "r");
  //if file stream is empty file could not be open or had no content
  if (fp == NULL) {
    perror("Error opening file.");
    return(NULL);
  }
  //fill up our struct with the data in the file stream
  fscanf(fp, "%lf %i %i",
	 &(m->default_value),
	 &(m->num_rows),
	 &(m->num_cols));
  int num_outliers = 0;
  double value;
  m->compact = (triple_t*) malloc (m->num_rows * m->num_cols * sizeof(triple_t));
  for (int i = 0; i < m->num_rows; i++){
    for (int j = 0; j < m->num_cols; j++){
      fscanf(fp, "%lf", &value);
      if (value != m->default_value){;
	initiate_triple(
			m->compact + num_outliers++, value, i, j);
      }
    }
  }
  m->compact_size = num_outliers;
  m->compact = realloc(m->compact, num_outliers * sizeof(triple_t));
  fclose(fp);
  return m;
}
void print_matrixkk (const matrixkk_t* r) {
  printf("([");
  print_triple(r->compact);
  for (int i = 1; i < r->compact_size; i++) {
    printf(", ");
    print_triple(r->compact + i);
  }
  printf("], %f, %i, %i)", r->default_value, r->num_rows, r->num_cols);
}
void sum_matrixkk (const matrixkk_t* r1,const matrixkk_t* r2, matrixkk_t* r3) {
  r3->default_value = r1->default_value + r2->default_value;
  merge_triple_except(r1->compact, r1->compact_size,
	       r2->compact, r2->compact_size,
	       r3->compact, &(r3->compact_size),
	       r3->default_value);
  r3->num_rows = r1->num_rows;
  r3->num_cols = r1->num_cols;
}
void transpose_matrixkk(const matrixkk_t* r, matrixkk_t* rt) {
  rt->compact_size = r->compact_size;
  triple_t tmp[rt->compact_size];
  transpose_triple_list(r->compact, rt->compact_size, tmp);
  mergesort_triple(tmp, rt->compact_size, rt->compact);
  rt->default_value = r->default_value;
  rt->num_rows = r->num_rows;
  rt->num_cols = r->num_cols;
}

void free_matrixkk (matrixkk_t* m) {
  for (int i = 0; i < m->num_rows; i++)
    free(&(m->compact[i]));
  free(m);
}
