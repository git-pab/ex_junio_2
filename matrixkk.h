#ifndef MATRIXKK_H
#define MATRIXKK_H
#include "tripla_aij.h"

typedef struct {
  triple_t* compact;
  int compact_size;
  double default_value;
  int num_rows;
  int num_cols;
} matrixkk_t;

void sum_matrixkk (const matrixkk_t* r1, const matrixkk_t* r2, matrixkk_t* r3);
void transpose_matrixkk(const matrixkk_t* r, matrixkk_t* rt);
matrixkk_t* read_matrixkk (const char* file_name);
void print_matrixkk (const matrixkk_t* r);
void free_matrixkk (matrixkk_t* m);

#endif //KASIKONSTANTE_H
