#include <stdio.h>
#include "svector.h"

void print_svector (svector_t* v) {
  printf("[");
  if (v->size)
    printf("%i", v->data[0]);
  for (int i = 1; i < v->size; i++)
    printf(", %i", v->data[i]);
  printf("]");
}
