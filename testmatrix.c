#include <stdio.h>
#include <stdlib.h>
#include "tripla_aij.h"
#include "matrixkk.h"

int main() {
  matrixkk_t kk1;
  kk1.num_rows = 4;
  kk1.num_cols = 4;
  kk1.default_value = 2;
  int m1_size = 4;
  triple_t m1[m1_size];
  m1[0] = *create_triple(1, 0, 0);
  m1[1]= *create_triple(5, 1, 1);
  m1[2] = *create_triple(3, 1, 2);
  m1[3] = *create_triple(8, 2, 2);
  kk1.compact = m1;
  kk1.compact_size = m1_size;
  print_matrixkk(&kk1);
  printf("\n");
  
  char filename[] = "mat.txt"; 
  matrixkk_t kk2 = *read_matrixkk(filename);
  print_matrixkk(&kk2);
  printf("\n");
  
  matrixkk_t kk3;
  kk3.compact_size = kk1.compact_size + kk2.compact_size;
  kk3.compact = (triple_t*) malloc (kk3.compact_size * sizeof(triple_t));

  matrixkk_t kk4;
  kk4.compact_size = kk2.compact_size;
  kk4.compact = (triple_t*) malloc (kk4.compact_size * sizeof(triple_t));
  transpose_matrixkk(&kk2, &kk4);
  print_matrixkk(&kk4);
  printf("\n");
  
  sum_matrixkk(&kk1, &kk2, &kk3);
  //We free the memory that we dont need in case we had allocated more
  //than necesary
  kk3.compact = realloc (kk3.compact, kk3.compact_size * sizeof(triple_t)); 
  print_matrixkk(&kk3);
  printf("\n");
  //rint_matrix_alt (mat_alt, num_rows, num_cols);
}
  
