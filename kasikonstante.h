#ifndef KASIKONSTANTE_H
#define KASIKONSTANTE_H
#include "tripla_aij.h"

typedef struct {
  triple_t* compacta;
  int valor_por_defecto;
  int filas;
  int columnas;
} kasikonstante_t;

kasikonstante_t* sum_reduced (kasikonstante_t* r1, kasikonstante_t* r2);

void print_reduced (kasikonstante_t r);

int*** quicksortadd (int*** unsorted);

#endif //KASIKONSTANTE_H
