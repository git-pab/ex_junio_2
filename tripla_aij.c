#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "tripla_aij.h"

//Prints the triplet just like it was a vector
//Cambia esto!
void print_triple (const triple_t* t) {
  printf("[%f", t->value);
  for (int i = 0; i < 2; i++)
    printf(", %i", t->pos[i]);
  printf("]");
}

//Allocates memory for a triplet
triple_t* create_triple (const double d, const int p1, const int p2) {
  triple_t* t = (triple_t*) malloc (sizeof(triple_t));
  t->value = d;
  t->pos[0] = p1;
  t->pos[1] = p2;
  return t;
}
void initiate_triple(triple_t* t, const double d, const int p1, const int p2) {
  t->value = d;
  t->pos[0] = p1;
  t->pos[1] = p2;
}

//Frees memory for a triplet
void destroy_triple (triple_t* t) {
  free(t);
}

//Adds a triplet first element if the rest of them have the same value
bool add_triple_p (triple_t t1, triple_t t2, triple_t* t3) {
  bool b = eq_triple (t1, t2);
  if (b) {
    t3->value = t2.value + t2.value;
    t3->pos[0] = t1.pos[0];
    t3->pos[1] = t1.pos[1];
  }
  return b;
}

//Adds a tripplet first cordinate w/o checking if the are equal.
triple_t* add_triple (triple_t t1, triple_t t2) {
  triple_t* t3 = create_triple(t1.value + t2.value, t1.pos[0], t1.pos[1]);
  return t3;
}

//Checks that a triplet is smaller than another using lexicographical
//order on the two last components
bool lt_triple (triple_t t1, triple_t t2) {
  bool b =
    t1.pos[0] < t2.pos[0] || (t1.pos[0] == t2.pos[0] && t1.pos[1] < t2.pos[1]);
  return b;
}

//Checks for equality analogous to the less than equal case.
bool eq_triple (triple_t t1, triple_t t2) {
  bool b = t1.pos[0] == t2.pos[0] && t1.pos[1] == t2.pos[1];
  return b;
}

//Checks for greater than analogous to the other cases.
bool gt_triple (triple_t t1, triple_t t2) {
  bool b = t1.pos[0] > t2.pos[0] || (t1.pos[0] == t2.pos[0] && t1.pos[1] > t2.pos[1]);
  return b;
}
void merge_triple (const triple_t* t1, int l1, const triple_t* t2, int l2, triple_t* t3) {
  int i1 = 0, i2 = 0, i3 = 0;
  while (i1 < l1 && i2 < l2) {
    if (lt_triple(t1[i1], t2[i2])) {
      t3[i3++] = t1[i1++];
    } else if (gt_triple(t1[i1], t2[i2])) {
      t3[i3++] = t2[i2++];
    } else {
      t3[i3++] = t1[i1++];
      t3[i3++] = t2[i2++];
    }
  }
  while (i1 < l1) {
    t3[i3++] = t1[i1++];
  }
  while (i2 < l2) {
    t3[i3++] = t2[i2++];
  }
}

void transpose_triple (const triple_t* t, triple_t* tt) {
  tt->value = t->value;
  tt->pos[0] = t->pos[1];
  tt->pos[1] = t->pos[0];
}

void transpose_triple_list(const triple_t* t, int len, triple_t* tt) {
  for (int i = 0; i < len; i++)
    transpose_triple(t + i, tt + i);
}

void mergesort_triple (const triple_t* u_list, int len, triple_t* s_list) {
  if (len == 1)
    s_list[0] = u_list[0];
  else {
    int l1 = len/2;
    triple_t p1[l1];
    int l2 = len - l1;
    triple_t p2[l2];
    mergesort_triple(u_list, l1, p1);
    mergesort_triple(u_list + l1, l2, p2);
    merge_triple(p1, l1, p2, l2, s_list);
  }
}

void merge_triple_except (const triple_t* t1, int l1, const triple_t* t2, int l2, triple_t* t3, int* l3, double exception) {
  int i1 = 0;
  int i2 = 0;
  int i3 = 0;
  while (i1 < l1 && i2 < l2) {
    if (lt_triple(t1[i1], t2[i2])) {
      if (t1[i1].value != exception) {
	t3[i3] = t1[i1];
	i3++;
      }
      i1++;
    }
    else if (gt_triple(t1[i1], t2[i2])) {
      if (t2[i2].value != exception) {
	t3[i3] = t2[i2];
	i3++;
      }
      i2++;
    }
    else {
      triple_t* maybe_m3 = add_triple(t1[i1], t2[i2]);
      if (maybe_m3->value != exception) {
	t3[i3] = *maybe_m3;
	i3++;
      }
      i1++;
      i2++;
    }
  }
  while (i1 < l1) {
    t3[i3] = t1[i1];
    i1++;
    i3++;
  }
  while (i2 < l2) {
    t3[i3] = t1[i2];
    i2++;
    i3++;
  }
  *l3 = i3;
}

