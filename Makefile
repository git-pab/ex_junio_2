CC=clang
CFLAGS=-g -Wall

all: testvector

svector.o: svector.c svector.h
	$(CC) $(CFLAGS) -c svector.c

vector.o: vector.c vector.h
	$(CC) $(CFLAGS) -c vector.c

matrix.o: matrix.c matrix.h
	$(CC) $(CFLAGS) -c matrix.c

matrixkk.o: matrixkk.c matrixkk.h
	$(CC) $(CFLAGS) -c $^

smatrix.o: smatrix.c smatrix.h
	$(CC) $(CFLAGS) -c smatrix.c

tripla_aij.o: tripla_aij.c tripla_aij.h vector.h
	$(CC) $(CFLAGS) -c tripla_aij.c

testsvector: svector.o testsvector.c
	$(CC) $(CFLAGS) -o testsvector testsvector.c svector.o

testvector: tripla_aij.o testvector.c
	$(CC) $(CFLAGS) -o testvector testvector.c tripla_aij.o

testmatrix: matrixkk.o tripla_aij.o testmatrix.c
	$(CC) $(CFLAGS) -o $@ $^

run_test_svector: testsvector
	./testsvector

run_test_vector: testvector
	./testvector

run_test_matrix: testmatrix
	./testmatrix
clean:
	rm *.o testvector
