#ifndef SMATRIX_H
#define SMATRIX_H

typedef struct {
  int** data;
  int num_rows;
  int num_cols;
} smatrix_t;

void print_matrix (smatrix_t* m);

#endif //SMATRIX_H
