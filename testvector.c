#include <stdlib.h>
#include <stdio.h>
#include "tripla_aij.h"

int main() {
  triple_t* vec1 = create_triple(3, 1, 2);
  triple_t* vec2 = create_triple(3, 0, 2);
  triple_t vec3;
  if (add_triple_p(vec1, vec2, &vec3))
    print_triple(vec3);
  else
    printf("Error.");
  printf("\n");
  triple_t* sol = add_triple(vec1,vec2);
  print_triple(*sol);
}
