#ifndef MATRIX_H
#define MATRIX_H

typedef int** matrix_t;

void print_matrix (int** m, int num_rows, int num_cols);

void print_matrix_alt (int* m, int num_rows, int num_cols);

#endif //MATRIX_H
