#include <stdio.h>
#include "matrix.h"

void print_matrix (matrix_t* m) {
  printf("[[%i", m->data[0][0]);
  for (int j = 1; j < m->num_cols; j++)
    printf(", %i", m->data[0][j]);
  printf("], ");
  for (int i = 1; i < m->num_rows; i++) {
    printf("[%i", m->data[i][0]);
    for (int j = 1; j < m->num_cols; j++) {
      printf(", %i", m->data[i][j] );
    }
    printf("]"
}
