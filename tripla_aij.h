#ifndef TRIPLA_AIJ_H
#define TRIPLA_AIJ_H
#include <stdbool.h>
#include "vector.h"

typedef struct {
  double value;
  int pos[2];
} triple_t;

void print_triple (const triple_t* t);
void initiate_triple (triple_t* t, const double d, const int p1, const int p2);
triple_t* create_triple (const double d, const int p1, const int p2);
void destroy_triple (triple_t* t);
bool add_triple_p (triple_t t1, triple_t t2, triple_t* t3);
triple_t* add_triple (triple_t t1, triple_t t2);
bool lt_triple (triple_t t1, triple_t t2);
bool eq_triple (triple_t t1, triple_t t2);
bool gt_triple (triple_t t1, triple_t t2);
void transpose_triple (const triple_t* t, triple_t* tt);
void transpose_triple_list (const triple_t* t, int len, triple_t* tt);
void merge_triple (const triple_t* t1, int l1,const triple_t* t2, int l2, triple_t* t3);
void mergesort_triple (const triple_t * ut, int l, triple_t* st);
void merge_triple_except (const triple_t* t1, int l1, const triple_t* t2, int l2, triple_t* t3, int* l3, double exception);

#endif //TRIPLA_AIJ_H
